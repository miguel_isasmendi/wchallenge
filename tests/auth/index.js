'use strict';
let authFunction = require('./../../auth/index');
let authInterceptor = null;

describe('Test auth algorithm', function() {
  before(function() {
    authInterceptor = authFunction({
      auth: {},
    });
  });

  describe('Token restriction NOT configured', function() {
    it('Can take NO auth headers', function(done) {
      authInterceptor({headers: {}}, () => {}, () => {});

      done();
    });

    it('Can take WITH auth headers', function(done) {
      authInterceptor(
        {headers: { authorization: 'Some token' }},
        () => {},
        () => {}
      );

      done();
    });
  });

  describe('Token restriction configured', function() {
    before(function() {
      authInterceptor = authFunction({
        auth: {
          allowed_token: 'A',
        },
      });
    });

    describe('Correct token received', function() {
      it('Receives a correct header', function(done) {
        authInterceptor(
          {headers: { authorization: 'Bearer A' }},
          () => {},
          () => {}
        );

        done();
      });
    });

    describe('Incorrect token received', function() {
      let errorString = 'TypeError: res.status is not a function';

      it('Receives a malformed header', function(done) {
        try {
          authInterceptor(
            {headers: { authorization: 'bad_string' }},
            () => {},
            () => {}
          );
        } catch (e) {
          if (e.toString() === errorString) {
            done();
          }
        }
      });

      it('Receives a wrong header', function(done) {
        try {
          authInterceptor(
            {headers: { authorization: 'Bearer bad_string' }},
            () => {},
            () => {}
          );
        } catch (e) {
          if (e.toString().indexOf(errorString) !== -1) {
            done();
          }
        }
      });
    });
  });
});
