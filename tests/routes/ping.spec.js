'use strict';
let request = require('supertest');
let authHeader = '';

const expect = require('chai').expect;

describe('GET /ping', () => {
  var server;
  before(() => {
    delete require.cache[require.resolve('../../app')];
    server = require('../../app');
    // TODO I should wait for the server being up waiting
    // for the server to send an event and start testing.
    authHeader = `Bearer ${global.gConfig.auth['allowed_token']}`;
  });

  after(done => {
    // TODO I should wait for the server being up waiting
    // for the server to send an event and start testing.
    setTimeout(() => { server.close(done); process.exit(0); }, 1000);
  });

  it('should return pong', done => {
    request(server)
      .get('/ping')
      .set({ Authorization: authHeader, Accept: 'application/json' })
      .expect(200)
      .expect('Content-Type', 'application/json; charset=utf-8')
      .end((err, res) => {
        expect(res.body.pong).to.be.true;
        done(err, res);
      });
  });
});
