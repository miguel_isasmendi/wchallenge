'use strict';

let logger = require('log4js').getLogger('DB');
let mongoose = require('mongoose');

let config = global.gConfig.database.mongo;
let dbUri = `mongodb://${config.host}:${config.port}/${config.name}`;
let connection = mongoose.createConnection(
  dbUri,
  {
    useNewUrlParser: true,
  }
);

connection.on('error', e => console.error(e));
connection.on('open', () => {
  logger.info(`DB connection started succesfully to ${dbUri}!!!!!`);
  require('../models').apply(connection, mongoose, {});
});

module.exports = {
  connection: connection,
  mongoose: mongoose,
};
