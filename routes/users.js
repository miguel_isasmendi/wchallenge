'use strict';

let router = require('express').Router();
let User = require('../models/User');
let validUrl = require('valid-url');

router.get('/', (req, res, next) => {
  User.find({}, (error, data) => {
    if (error) return next(error);
    res.json(data);
  });
});

router.get('/:id', (req, res, next) => {
  User.findById(req.params.id, (error, data) => {
    if (error) return next(error);

    if (!data) {
      res.status(404);
      res.send();
    } else {
      res.json(data);
    }
  });
});

router.post('/', (req, res, next) => {

  if (!req.body.avatar || !validUrl.isUri(req.body.avatar)) {
    res.status(422);
    res.send();
    return;
  }

  User.create({
    name: req.body.name,
    avatar: req.body.avatar,
  }, (error, data) => {
    if (error) return next(error);

    res.json(data);
  });
});

router.patch('/:id', (req, res, next) => {

  let newData = {};

  if (Object.keys(req.body).length === 0) {
    res.status(422);
    res.send();
    return;
  }

  if (req.body.name) {
    newData.name = req.body.name;
  }

  if (req.body.avatar) {
    if (validUrl.isUri(req.body.avatar)) {
      newData.avatar = req.body.avatar;
    } else {
      res.status(422);
      res.send();
      return;
    }
  }

  if (Object.keys(newData).length === 0) {
    res.status(422);
    res.send();
    return;
  }

  User.findByIdAndUpdate(
    req.params.id,
    newData,
    {
      new: true,
    }, (error, data) => {
      if (error) return next(error);

      if (!data) {
        res.status(404);
        res.send();
      } else {
        res.json(data);
      }
    });
});

module.exports = router;
