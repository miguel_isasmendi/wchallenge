'use strict';

let router = require('express').Router();

router.get('/', (req, res) => {
  res.send({pong: true});
});

module.exports = router;
