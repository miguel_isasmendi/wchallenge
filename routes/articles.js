'use strict';

const router = require('express').Router();
const Article = require('../models/Article');

router.get('/:id', (req, res, next) => {
  Article.findById(
    req.params.id,
    (err, data) => {
      if (err) return next(err);

      if (!data) {
        res.status(404);
        res.send();
      } else {
        res.json(data);
      }
    });
});

router.get('/', (req, res, next) => {
  let criteria = {};

  if (req.query.tags && req.query.tags.length > 0) {
    criteria.tags = { "$in" : [...new Set(req.query.tags.split(','))] };
  }

  Article.find(criteria, (err, data) => {
    if (err) return next(err);
    res.json(data);
  });
});

router.post('/', (req, res, next) => {
  Article.create({
    user: req.body.userId,
    title: req.body.title,
    text: req.body.text,
    tags: req.body.tags || [],
  },
  (err, data) => {
    if (err) return next(err);
    res.json(data);
  });
});

router.patch('/:id', (req, res, next) => {
  let newData = {};

  if (Object.keys(req.body).length === 0) {
    res.status(422);
    res.send();
    return;
  }

  if (req.body.title) {
    newData.title = req.body.title;
  }

  if (req.body.text) {
    newData.text = req.body.text;
  }

  if (req.body.userId !== null && req.body.userId !== undefined) {
    newData.user = req.body.userId;
  }

  if (req.body.tags) {
    newData.tags = [...new Set(req.body.tags)];
  }

  if (Object.keys(newData).length === 0) {
    res.status(422);
    res.send();
    return;
  }

  Article.findByIdAndUpdate(
    req.params.id,
    newData,
    {
      new: true,
    },
    (err, data) => {
      if (err) return next(err);

      if (!data) {
        res.status(404);
        res.send();
      } else {
        res.json(data);
      }
    });
});

router.delete('/:id', (req, res, next) => {

  Article.findByIdAndRemove(
    req.params.id,
    (err, data) => {
      if (err) return next(err);

      if (!data) {
        res.status(404);
        res.send();
      } else {
        res.json(data);
      }
    });

});

module.exports = router;
