'use strict';

let logger = require('log4js').getLogger('Routes');
let router = require('express').Router();

logger.info('Loading routes...');

router.use('/ping', require('./ping'));
router.use('/user', require('./users'));
router.use('/article', require('./articles'));

logger.info('Loading routes finished!!!');
// logger.info(router.stack);

module.exports = router;
