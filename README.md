# Challenge

## Project setup
```
npm install
```
This project uses MongoDB as storage and assumes the host has it installed accordingly (See [MongoDB installation](https://docs.mongodb.com/manual/installation/) ).

The mongoDB instance is assumed to be located in localhost and listening to port 27017. Upon usage it creates a new db called challenge (if it wasn't present already).


## Starting the server
```
npm start
```
This command starts the server in port 300 in dev environment.

## Scripts
```
start
test -> Starts the tests
pretest -> runs eslint for this project
eslint-fix -> runs eslint --fix fixing format issues.
```

## Authorization

The endpoints are protected requested authorication by checking the Bearer token against a default value in the configuration file. But the token value ends up equals to **AUTH_TOKEN** environment variable. Else, we use a default value held in config file.

For more flexibility we could even take out this attribute from configuration and the application will atone to it.

## Endpoints
### User
- Create a new user
- Create a new article
- Edit an article
- Delete an article
- Return all articles (from all users) that contains the given tag(s) (1 or more).

There are more endpoints available, that are described in the exported file as postman exports.
