
'use strict';

module.exports = {
  development: {
    config_id: 'development',
    node_port: 3000,
    auth: {
      allowed_token: process.env.AUTH_TOKEN || '5CD4ED173E1C95FE763B753A297D5',
    },
    database: {
      mongo: {
        host: 'localhost',
        port: 27017,
        name: 'challenge',
      },
    },
  },
  testing: {
    config_id: 'testing',
  },
  production: {
    config_id: 'production',
    node_port: 8080,
  },
};
