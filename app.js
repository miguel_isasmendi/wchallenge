'use strict';
const express = require('express');
const _ = require('lodash');
const log4js = require('log4js');
const logger = log4js.getLogger('App');

// module variables
const config = require('./config');
const environment = process.env.NODE_ENV || 'development';
const environmentConfig = config[environment];

global.gConfig = _.merge(config.development, environmentConfig);

// creating express instance
let app = express();

// configuring logger
log4js.configure({
  appenders: { out: { type: 'stdout' } },
  categories: { default: { appenders: ['out'], level: 'info' } },
});
app.use(log4js.connectLogger(log4js.getLogger('http'), { level: 'auto' }));

let bodyParser = require('body-parser');

// configuring express
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Adding token verification behavior
app.use(require('./auth')(global.gConfig));

// Adding routes
let router = require('./routes');

app.use('/', router);

// Starting db configurations
require('./dbs');

// Starting server
module.exports = app
  .listen(
    global.gConfig.node_port,
    () => logger.info(`Server started at port: ${global.gConfig.node_port}`));
