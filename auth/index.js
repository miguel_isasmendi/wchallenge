'use strict';
module.exports = config => {
  let allowedToken = config.auth['allowed_token'];

  return (req, res, next) => {

    if (allowedToken) {
      if (!req.headers.authorization) {
        return res.status(403).json({ error: 'No credentials sent!' });
      }

      if (req.headers.authorization !== `Bearer ${allowedToken}`) {
        return res.status(401).json({ error: 'Unauthorized' });
      }
    }

    next();
  };
};
