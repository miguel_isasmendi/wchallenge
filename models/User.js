'use strict';

let db = require('../dbs');

let userSchema = db.mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true,
  },
  avatar: {
    type: String,
    trim: true,
    required: true,
  },
});

module.exports = db.connection.model('User', userSchema);
