'use strict';

let logger = require('log4js').getLogger('Models');

module.exports = function createModels(connection, mongoose) {
  logger.info('Creating Models...');

  require('./User');
  require('./Article');

  logger.info('Models created!!!');
};
