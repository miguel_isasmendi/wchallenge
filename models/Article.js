'use strict';

let db = require('../dbs');

var ArticleSchema = db.mongoose.Schema({
  user: {
    type: db.mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  title: {
    type: String,
    trim: true,
    required: true,
  },
  text: {
    type: String,
    trim: true,
    required: true,
  },
  tags: [{
    type: String,
    trim: true,
  }],
});

module.exports = db.connection.model('Article', ArticleSchema);
